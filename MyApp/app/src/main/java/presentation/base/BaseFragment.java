package presentation.base;

import android.app.Fragment;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.filipemp.myapp.R;

import support.Logger;

/**
 * Created by filipe.mp on 7/5/17.
 */

public abstract class BaseFragment extends Fragment implements Lifecycle.View {

    protected abstract Lifecycle.ViewModel getViewModel();
    ProgressBar loading;

    protected View root;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = init(inflater, container);
        setupProgressBar();
        setupToolbar();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        getViewModel().onViewResumed();
    }

    @Override
    public void onStart() {
        super.onStart();
        getViewModel().onViewAttached(this);
        requestData();
    }

    @Override
    public void onStop() {
        super.onStop();
        getViewModel().onViewDetached();
    }

    public abstract View init(LayoutInflater inflater, @Nullable ViewGroup container);

    public abstract void requestData();

    public abstract void toolbarClicked();

    public abstract boolean hasMenu();

    public ProgressBar getLoading() {
        if (loading == null) {
            Logger.e(getString(R.string.progress_bar_layout_error));
        }
        return loading;
    }

    private void setupProgressBar() {
        loading = root.findViewById(R.id.loading);

        if (loading == null) {
            Logger.e(getString(R.string.progress_bar_layout_error));
        } else {
            int color = getResources().getColor(R.color.colorPrimary);
            loading.getIndeterminateDrawable()
                    .setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        }
    }

    private void setupToolbar() {
        Toolbar myToolbar = root.findViewById(R.id.toolbar);
        if (myToolbar != null && getActivity() instanceof AppCompatActivity) {
            AppCompatActivity activity = (AppCompatActivity) getActivity();
            activity.setSupportActionBar(myToolbar);

            myToolbar.setOnClickListener(v -> toolbarClicked());

            if (activity.getSupportActionBar() != null && hasMenu()) {
                ActionBar ab = activity.getSupportActionBar();
                ab.setHomeAsUpIndicator(R.drawable.ic_menu);
                ab.setDisplayHomeAsUpEnabled(true);
            }
        } else {
            Logger.e(getString(R.string.toolbar_layout_error));
        }
    }

    public void showSnackbarAPIError() {
        String message = getString(R.string.generic_api_error);
        View view = root.findViewById(R.id.coordinatorLayout);
        if (view == null) {
            Logger.e(getString(R.string.coordinator_layout_error));
        } else {
            showSnackbar(view, message);
        }
    }

    public void showSnackbar(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT)
                .show();
    }
}

