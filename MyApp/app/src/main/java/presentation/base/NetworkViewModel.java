package presentation.base;

import android.support.annotation.CallSuper;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableMaybeObserver;
import support.NetworkConstants;

import static support.NetworkConstants.REQUEST_FAILED;
import static support.NetworkConstants.REQUEST_NONE;
import static support.NetworkConstants.REQUEST_RUNNING;
import static support.NetworkConstants.REQUEST_SUCCEEDED;

/**
 * Created by filipe.mp on 6/6/17.
 */

public abstract class NetworkViewModel<Repository> {

    protected @NetworkConstants.RequestState
    int requestState;
    protected Throwable lastError;

    protected Repository repository;
    protected CompositeDisposable compositeDisposable;

    public abstract boolean isRequestingData();

    public NetworkViewModel(Repository repository) {
        this.repository = repository;
        this.requestState = REQUEST_NONE;
        compositeDisposable = new CompositeDisposable();
    }

    public @NetworkConstants.RequestState
    int getRequestState() {

        if (isRequestingData()) {
            return REQUEST_RUNNING;
        }

        return requestState;
    }

    public Throwable getLastError() {

        return lastError;
    }

    protected class MaybeNetworkObserver<T> extends DisposableMaybeObserver<T> {

        @Override
        @CallSuper
        public void onSuccess(T value) {

            requestState = REQUEST_SUCCEEDED;
        }

        @Override
        @CallSuper
        public void onError(Throwable e) {

            lastError = e;
            requestState = REQUEST_FAILED;
        }

        @Override
        public void onComplete() {

        }
    }
}
