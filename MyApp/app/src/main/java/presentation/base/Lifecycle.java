package presentation.base;

import android.support.annotation.NonNull;

/**
 * Created by filipe.mp on 6/6/17.
 */

public interface Lifecycle {

    interface View {

    }

    interface ViewModel {

        void onViewResumed();
        void onViewAttached(@NonNull Lifecycle.View viewCallback);
        void onViewDetached();
    }
}
