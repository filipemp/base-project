package presentation.movies;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.example.filipemp.myapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.navigation)
    NavigationView navigationView;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        navigationView.setNavigationItemSelectedListener(this);

        getFragmentManager().beginTransaction()
                .add(R.id.fragment_placeholder, new MoviesFragment())
                .commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int itemId = item.getItemId();
        Fragment frag = new MoviesFragment();
        Bundle args = null;

        if (itemId == R.id.now_playing) {
            args = MoviesBundleManager.getBundle(false);
        } else if (itemId == R.id.bookmarks) {
            args = MoviesBundleManager.getBundle(true);
        }

        frag.setArguments(args);

        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_placeholder, frag)
                .commit();

        drawerLayout.closeDrawers();

        return true;
    }

    @Override

    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();

        switch (itemId) {

            case android.R.id.home: {
                drawerLayout.openDrawer(GravityCompat.START);
                return true;

            }

        }

        return false;
    }
}
