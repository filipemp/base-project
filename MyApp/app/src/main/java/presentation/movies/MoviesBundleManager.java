package presentation.movies;

import android.os.Bundle;

/**
 * Created by filipe.mp on 11/4/17.
 */

public class MoviesBundleManager {

    private static final String IS_BOOKMARKS_KEY = "isBookmarksKey";

    protected static Bundle getBundle(boolean isBookmarks) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(IS_BOOKMARKS_KEY, isBookmarks);
        return bundle;
    }

    protected static boolean getIsBookmarks(Bundle bundle) {
        if (bundle == null) {
            return false;
        }

        return bundle.getBoolean(IS_BOOKMARKS_KEY, true);
    }

}
