package presentation.movies;

import java.util.List;

import presentation.base.Lifecycle;
import presentation.movies.models.MovieResult;

/**
 * Created by filipe.mp on 6/6/17.
 */

public interface MoviesContract {

    interface View extends Lifecycle.View {
        void showNowPlayingMovieList(List<MovieResult> movies);
        void showErrorMessage(String message);
        void showLoading();
    }

    interface ViewModel extends Lifecycle.ViewModel {
        void getNowPlayingMovieList();
        void getBookmarks();
    }

}
