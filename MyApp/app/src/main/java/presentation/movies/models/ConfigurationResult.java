package presentation.movies.models;

/**
 * Created by filipe.mp on 6/8/17.
 */

public class ConfigurationResult {

    private String basePosterUrl;
    private String baseBackdropUrl;

    public ConfigurationResult(String basePosterUrl, String baseBackdropUrl) {
        this.basePosterUrl = basePosterUrl;
        this.baseBackdropUrl = baseBackdropUrl;
    }

    public String getBasePosterUrl() {
        return basePosterUrl;
    }

    public String getBaseBackdropUrl() {
        return baseBackdropUrl;
    }
}
