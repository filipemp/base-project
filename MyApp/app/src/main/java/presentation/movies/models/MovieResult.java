package presentation.movies.models;

/**
 * Created by filipe.mp on 4/23/17.
 */

public class MovieResult {

    private String posterPath;
    private String backdropPath;
    private String overview;
    private Integer id;
    private String title;
    private Double voteAverage;

    public MovieResult(String posterPath, String overview, Integer id, String title,
                       Double voteAverage, String backdropPath) {
        this.posterPath = posterPath;
        this.overview = overview;
        this.id = id;
        this.title = title;
        this.voteAverage = voteAverage;
        this.backdropPath = backdropPath;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public String getOverview() {
        return overview;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Double getVoteAverage() {
        return voteAverage;
    }

    public String getBackdropPath() {
        return backdropPath;
    }
}
