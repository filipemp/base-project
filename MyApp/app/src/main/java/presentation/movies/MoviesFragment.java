package presentation.movies;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.filipemp.myapp.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import presentation.base.BaseFragment;
import presentation.lists.movies.EndlessRecyclerViewScrollListener;
import presentation.lists.movies.MoviesOnItemTouchListener;
import presentation.lists.movies.VerticalListAdapter;
import presentation.moviedetail.MovieDetailActivity;
import presentation.movies.models.MovieResult;
import support.Injection;
import support.Logger;

/**
 * Created by filipe.mp on 7/5/17.
 */

public class MoviesFragment extends BaseFragment implements MoviesContract.View {

    @BindView(R.id.movie_list)
    RecyclerView recyclerView;

    private VerticalListAdapter adapter;
    private MoviesContract.ViewModel viewModel;
    private List<MovieResult> movies;
    private LinearLayoutManager layoutManager;

    private boolean isBookmarks;
    private boolean isBookmarksLoaded;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public View init(LayoutInflater inflater, ViewGroup container) {
        View root = inflater.inflate(R.layout.fragment_movies, container, false);

        ButterKnife.bind(this, root);

        isBookmarksLoaded = false;
        viewModel = Injection.provideMoviesViewModel();
        movies = new ArrayList<>();
        adapter = new VerticalListAdapter(getActivity(), movies);
        layoutManager = new LinearLayoutManager(getActivity());

        RecyclerView.OnScrollListener scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(RecyclerView view) {
                if (!isBookmarks) {
                    requestData();
                }
            }
        };

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(scrollListener);
        recyclerView.addOnItemTouchListener(new MoviesOnItemTouchListener(getActivity(), recyclerView,
                movies, this::onMovieClick));

        return root;
    }

    @Override
    public void requestData() {
        isBookmarks = MoviesBundleManager.getIsBookmarks(getArguments());
        if (isBookmarks && !isBookmarksLoaded) {
            viewModel.getBookmarks();
        } else if (!isBookmarks) {
            viewModel.getNowPlayingMovieList();
        }
    }

    @Override
    public void toolbarClicked() {
        RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(getActivity()) {
            @Override protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };

        smoothScroller.setTargetPosition(0);
        layoutManager.startSmoothScroll(smoothScroller);
    }

    @Override
    public boolean hasMenu() {
        return true;
    }

    @Override
    public MoviesContract.ViewModel getViewModel() {
        return viewModel;
    }

    private void onMovieClick(MovieResult movieResult) {
        Bundle bundle = MovieDetailActivity.getBundle(movieResult.getId());
        Intent intent = new Intent(getActivity(), MovieDetailActivity.class);
        intent.putExtras(bundle);
        getActivity().startActivity(intent);
    }

    public void showNowPlayingMovieList(List<MovieResult> movieResults) {
        isBookmarksLoaded = true;
        showMovieListUI();

        movies.addAll(movieResults);
        adapter.notifyDataSetChanged();
    }

    private void showMovieListUI() {
        getLoading().setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoading() {
        getLoading().setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMessage(String message) {
        Logger.e(message);
        showSnackbarAPIError();
    }
}
