package presentation.movies;

import android.support.annotation.NonNull;

import java.util.List;

import data.movies.MoviesDataContract;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import presentation.base.Lifecycle;
import presentation.base.NetworkViewModel;
import presentation.movies.models.MovieResult;
import support.NetworkConstants;

import static support.NetworkConstants.REQUEST_FAILED;
import static support.NetworkConstants.REQUEST_SUCCEEDED;

/**
 * Created by filipe.mp on 6/6/17.
 */

public class MoviesViewModel extends NetworkViewModel<MoviesDataContract.Repository>
        implements MoviesContract.ViewModel {

    private Integer page;
    private MoviesContract.View viewCallback;
    private List<MovieResult> lastMovies;

    public MoviesViewModel(MoviesDataContract.Repository moviesRepository) {
        super(moviesRepository);
        page = 1;
    }

    @Override
    public boolean isRequestingData() {
        return repository.isRequestingData();
    }

    @Override
    public void onViewResumed() {
        @NetworkConstants.RequestState int requestState = getRequestState();
        if (requestState == REQUEST_SUCCEEDED) {
            viewCallback.showNowPlayingMovieList(lastMovies);
        } else if (requestState == REQUEST_FAILED) {
            viewCallback.showErrorMessage(getLastError().getMessage());
        }
    }

    @Override
    public void onViewAttached(@NonNull Lifecycle.View viewCallback) {
        this.viewCallback = (MoviesContract.View) viewCallback;
    }

    @Override
    public void onViewDetached() {
        viewCallback = null;
        compositeDisposable.clear();
    }

    @Override
    public void getNowPlayingMovieList() {
        if (page == 1) {
            viewCallback.showLoading();
        }

        Disposable disposable = repository.getNowPlayingMovies(page)
                .doOnSuccess(nowPlayingResponse -> incrementPageCounter())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new MoviesSubscriber());

        compositeDisposable.add(disposable);
    }

    @Override
    public void getBookmarks() {
        viewCallback.showLoading();

        Disposable disposable = repository.getBookmarks()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((movieResults, throwable) -> {
                    if (throwable == null) {
                        lastMovies = movieResults;
                        viewCallback.showNowPlayingMovieList(movieResults);
                    } else {
                        viewCallback.showErrorMessage(throwable.getMessage());
                    }
                });

        compositeDisposable.add(disposable);
    }

    private void incrementPageCounter() {
        page++;
    }

    private class MoviesSubscriber extends MaybeNetworkObserver<List<MovieResult>> {


        @Override
        public void onSuccess(List<MovieResult> value) {
            super.onSuccess(value);
            lastMovies = value;
            viewCallback.showNowPlayingMovieList(value);
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
            viewCallback.showErrorMessage(e.getMessage());
        }

        @Override
        public void onComplete() {

        }
    }
}
