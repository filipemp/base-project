package presentation.moviedetail;

import android.support.annotation.NonNull;

import com.example.filipemp.myapp.R;

import data.movies.MoviesDataContract;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import presentation.base.Lifecycle;
import presentation.base.NetworkViewModel;
import presentation.moviedetail.models.MovieDetailResult;
import support.NetworkConstants;

import static support.NetworkConstants.REQUEST_FAILED;
import static support.NetworkConstants.REQUEST_SUCCEEDED;

/**
 * Created by filipe.mp on 6/12/17.
 */

public class MovieDetailViewModel extends NetworkViewModel<MoviesDataContract.Repository>
        implements MovieDetailContract.ViewModel {

    private MovieDetailContract.View viewCallback;

    private boolean isBookmarked;
    private MovieDetailResult movieDetailResult;

    public MovieDetailViewModel(MoviesDataContract.Repository movieRepository) {
        super(movieRepository);
        isBookmarked = false;
    }

    @Override
    public void onViewResumed() {
        @NetworkConstants.RequestState int requestState = getRequestState();
        if (requestState == REQUEST_SUCCEEDED) {
            viewCallback.showMovieDetail(movieDetailResult);
        } else if (requestState == REQUEST_FAILED) {
            viewCallback.showErrorMessage(getLastError().getMessage());
        }
    }

    @Override
    public void onViewAttached(@NonNull Lifecycle.View viewCallback) {
        this.viewCallback = (MovieDetailContract.View) viewCallback;
    }

    @Override
    public void onViewDetached() {
        viewCallback = null;
        compositeDisposable.clear();
    }

    @Override
    public void getMovieDetail(Integer id) {
        viewCallback.showLoading();

        Disposable disposable = repository.getMovieDetail(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new MovieDetailSubscribe());

        compositeDisposable.add(disposable);
    }

    @Override
    public void isBookmarked(Integer id) {
        Disposable disposable = repository.isBookmarked(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::setBookmarked);
        compositeDisposable.add(disposable);
    }

    private void setBookmarked(boolean isBookmarked) {
        this.isBookmarked = isBookmarked;

        int resId = isBookmarked ? R.string.remove_button :
                R.string.bookmark_button;
        callbackViewWithResourceId(resId);
    }

    private void callbackViewWithResourceId(int resourceId) {
        viewCallback.setBookmarkButtonText(resourceId);
        viewCallback.showBookmarkButton();
    }

    @Override
    public void buttonTouched() {
        if (isBookmarked) {
            removeIt(movieDetailResult, getBookmarkCompletionAction());
        } else {
            bookmarkIt(movieDetailResult, getBookmarkCompletionAction());
        }
    }

    private void bookmarkIt(MovieDetailResult movieDetailResult, Action completionAction) {
        runMovieOperation(repository.saveMovie(movieDetailResult), completionAction);
    }

    private void removeIt(MovieDetailResult movieDetailResult, Action completionAction) {
        runMovieOperation(repository.deleteMovie(movieDetailResult), completionAction);
    }

    private void runMovieOperation(Completable operation, Action completionAction) {
        Disposable disposable = operation.observeOn(AndroidSchedulers.mainThread())
                .subscribe(completionAction);
        compositeDisposable.add(disposable);
    }

    private Action getBookmarkCompletionAction() {
        return () -> viewCallback.setBookmarkButtonText(updateBookmarkButtonText());
    }

    private int updateBookmarkButtonText() {
        isBookmarked = !isBookmarked;
        return isBookmarked ? R.string.remove_button : R.string.bookmark_button;
    }

    @Override
    public boolean isRequestingData() {
        return repository.isRequestingData();
    }

    private class MovieDetailSubscribe extends MaybeNetworkObserver<MovieDetailResult> {

        @Override
        public void onSuccess(MovieDetailResult value) {
            super.onSuccess(value);
            movieDetailResult = value;
            viewCallback.showMovieDetail(movieDetailResult);
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
            viewCallback.showErrorMessage(e.getMessage());
        }

        @Override
        public void onComplete() {
        }
    }

}
