package presentation.moviedetail;

import presentation.base.Lifecycle;
import presentation.moviedetail.models.MovieDetailResult;

/**
 * Created by filipe.mp on 6/12/17.
 */

public interface MovieDetailContract {

    interface View extends Lifecycle.View {
        void showMovieDetail(MovieDetailResult movieDetailResult);
        void showErrorMessage(String message);
        void showLoading();
        void showBookmarkButton();
        void setBookmarkButtonText(int resourceId);
    }

    interface ViewModel extends Lifecycle.ViewModel {
        void getMovieDetail(Integer id);
        void buttonTouched();
        void isBookmarked(Integer id);
    }
}
