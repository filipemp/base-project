package presentation.moviedetail.models;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import support.DateFormatter;

/**
 * Created by filipe.mp on 6/12/17.
 */

public class MovieDetailResult {

    private Integer id;
    private String title;
    private String posterPath;
    private String backdropPath;
    private Double rating;
    private String overview;
    private String releaseDate;
    private List<GenreResult> genres;
    private List<String> genreNames;

    public MovieDetailResult(Integer id, String title, String posterPath, String backdropPath, Double rating,
                             String overview, String releaseDate, List<GenreResult> genres) {
        this.id = id;
        this.title = title;
        this.posterPath = posterPath;
        this.backdropPath = backdropPath;
        this.rating = rating;
        this.overview = overview;
        this.releaseDate = releaseDate;
        this.genres = genres;

        genreNames = new ArrayList<>();
        for (GenreResult genreResult : genres) {
            genreNames.add(genreResult.getName());
        }
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public Double getRating() {
        return rating;
    }

    public String getOverview() {
        return overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public String getReleaseDateFormatted(Context context) {
        DateFormatter dateFormatter = new DateFormatter(context);

        return dateFormatter.format(releaseDate);
    }

    public List<GenreResult> getGenres() {
        return genres;
    }

    public List<String> getGenreNames() {
        return genreNames;
    }
}
