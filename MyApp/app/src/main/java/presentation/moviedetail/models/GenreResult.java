package presentation.moviedetail.models;

/**
 * Created by filipe.mp on 7/6/17.
 */

public class GenreResult {

    private Integer id;

    private String name;

    public GenreResult(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
