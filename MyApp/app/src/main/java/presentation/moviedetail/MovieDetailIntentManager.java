package presentation.moviedetail;

import android.os.Bundle;

/**
 * Created by filipe.mp on 6/12/17.
 */

public class MovieDetailIntentManager {

    private static final String ID_KEY = "idKey";

    protected static Bundle getBundle(int id) {
        Bundle bundle = new Bundle();
        bundle.putInt(ID_KEY, id);
        return bundle;
    }

    protected static int getId(Bundle bundle) {
        return bundle.getInt(ID_KEY, 0);
    }
}
