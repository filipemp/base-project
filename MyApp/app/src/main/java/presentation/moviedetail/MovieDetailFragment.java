package presentation.moviedetail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.filipemp.myapp.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import presentation.base.BackFragment;
import presentation.base.Lifecycle;
import presentation.lists.genres.GridAdapter;
import presentation.moviedetail.models.MovieDetailResult;
import support.ImageRenderer;
import support.Injection;
import support.Logger;

/**
 * Created by filipe.mp on 7/5/17.
 */

public class MovieDetailFragment extends BackFragment implements MovieDetailContract.View {

    private MovieDetailContract.ViewModel viewModel;

    @BindView(R.id.movie_detail_container_view)
    View containerView;

    @BindView(R.id.movie_poster)
    ImageView moviePoster;

    @BindView(R.id.movie_title)
    TextView movieTitle;

    @BindView(R.id.movie_rating)
    RatingBar movieRating;

    @BindView(R.id.movie_overview)
    TextView movieOverview;

    @BindView(R.id.movie_release_date)
    TextView movieReleaseDate;

    @BindView(R.id.movie_genres)
    RecyclerView movieGenresRecyclerView;

    @BindView(R.id.bookmark_it_button)
    Button bookmarkItButton;

    private GridAdapter adapter;
    private List<String> genres;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public View init(LayoutInflater inflater, @Nullable ViewGroup container) {
        View root = inflater.inflate(R.layout.fragment_movie_detail, container,
                false);

        ButterKnife.bind(this, root);

        viewModel = Injection.provideMovieDetailViewModel();

        genres = new ArrayList<>();
        adapter = new GridAdapter(genres);

        movieGenresRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        movieGenresRecyclerView.setAdapter(adapter);
        movieGenresRecyclerView.setNestedScrollingEnabled(false);

        return root;
    }

    @Override
    public void requestData() {
        int movieId = MovieDetailIntentManager.getId(getArguments());
        viewModel.getMovieDetail(movieId);
        viewModel.isBookmarked(movieId);
    }

    @Override
    public void toolbarClicked() {}

    @Override
    protected Lifecycle.ViewModel getViewModel() {
        return viewModel;
    }

    @Override
    public void showMovieDetail(MovieDetailResult movieDetailResult) {
        showMovieDetailInfoUI();

        ImageRenderer.render(
                getActivity(),
                moviePoster,
                movieDetailResult.getPosterPath(),
                R.drawable.placeholder_poster,
                R.drawable.error_poster);

        movieTitle.setText(movieDetailResult.getTitle());
        movieRating.setRating(movieDetailResult.getRating().floatValue());
        movieOverview.setText(getString(R.string.movie_item_overview, movieDetailResult.getOverview()));
        movieReleaseDate.setText(getString(R.string.movie_item_release_date,
                movieDetailResult.getReleaseDateFormatted(getActivity())));

        genres.addAll(movieDetailResult.getGenreNames());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showErrorMessage(String message) {
        Logger.e(message);
        showSnackbarAPIError();
    }

    @Override
    public void showLoading() {
        getLoading().setVisibility(View.VISIBLE);
        containerView.setVisibility(View.GONE);
    }

    private void showMovieDetailInfoUI() {
        getLoading().setVisibility(View.GONE);
        containerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showBookmarkButton() {
        bookmarkItButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void setBookmarkButtonText(int resourceId) {
        bookmarkItButton.setText(getString(resourceId));
    }

    @OnClick(R.id.bookmark_it_button)
    public void bookmarkItClick() {
        viewModel.buttonTouched();
    }

}
