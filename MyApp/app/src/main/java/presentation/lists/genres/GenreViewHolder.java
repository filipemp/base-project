package presentation.lists.genres;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.filipemp.myapp.R;

/**
 * Created by filipe.mp on 6/20/17.
 */

public class GenreViewHolder extends RecyclerView.ViewHolder {

    public TextView genreTextView;

    public GenreViewHolder(View itemView) {
        super(itemView);

        genreTextView = itemView.findViewById(R.id.genre_text_view);
    }
}
