package presentation.lists.genres;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.filipemp.myapp.R;

import java.util.List;

/**
 * Created by filipe.mp on 6/20/17.
 */

public class GridAdapter extends RecyclerView.Adapter<GenreViewHolder> {

    private List<String> genres;

    public GridAdapter(List<String> genres) {
        this.genres = genres;
    }

    @Override
    public GenreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View genreItemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.genre_list_item, parent, false);

        return new GenreViewHolder(genreItemView);
    }

    @Override
    public void onBindViewHolder(GenreViewHolder holder, int position) {
        holder.genreTextView.setText(genres.get(position));
    }

    @Override
    public int getItemCount() {
        return genres.size();
    }
}
