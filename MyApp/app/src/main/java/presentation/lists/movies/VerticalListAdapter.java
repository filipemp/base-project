package presentation.lists.movies;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.filipemp.myapp.R;

import java.util.List;

import presentation.movies.models.MovieResult;

/**
 * Created by filipe.mp on 6/8/17.
 */

public class VerticalListAdapter extends RecyclerView.Adapter<MovieItemViewHolder> {

    private List<MovieResult> movies;
    private Context context;

    public VerticalListAdapter(Context context, List<MovieResult> movies) {
        this.context = context;
        this.movies = movies;
    }

    @Override
    public MovieItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View movieItemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_item, parent, false);

        return new MovieItemViewHolder(movieItemView);
    }

    @Override
    public void onBindViewHolder(MovieItemViewHolder holder, int position) {
        MovieResult movieResult = movies.get(position);
        ItemBinder.bind(context, holder, movieResult);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }
}
