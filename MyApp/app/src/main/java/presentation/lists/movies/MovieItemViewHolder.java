package presentation.lists.movies;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.filipemp.myapp.R;

/**
 * Created by filipe.mp on 6/8/17.
 */

public class MovieItemViewHolder extends RecyclerView.ViewHolder {

    public ImageView moviePoster;
    public TextView movieTitle;
    public RatingBar movieRating;

    public MovieItemViewHolder(View itemView) {
        super(itemView);
        moviePoster = itemView.findViewById(R.id.movie_poster);
        movieTitle = itemView.findViewById(R.id.movie_title_content);
        movieRating = itemView.findViewById(R.id.movie_rating);
    }
}
