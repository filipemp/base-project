package presentation.lists.movies;

import android.content.Context;

import com.example.filipemp.myapp.R;

import presentation.movies.models.MovieResult;
import support.ImageRenderer;

/**
 * Created by filipe.mp on 6/8/17.
 */

public class ItemBinder {

    static void bind(Context context, MovieItemViewHolder viewHolder, MovieResult movieResult) {
        viewHolder.movieRating.setRating(movieResult.getVoteAverage().floatValue());
        viewHolder.movieTitle.setText(movieResult.getTitle());

        ImageRenderer.renderWithFit(
                context,
                viewHolder.moviePoster,
                movieResult.getBackdropPath(),
                R.drawable.placeholder_backdrop,
                R.drawable.error_backdrop);
    }
}
