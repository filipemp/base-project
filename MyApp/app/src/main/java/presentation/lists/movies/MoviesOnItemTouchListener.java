package presentation.lists.movies;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import presentation.lists.base.RecyclerItemClickListener;
import presentation.movies.models.MovieResult;

/**
 * Created by filipe.mp on 6/12/17.
 */

public class MoviesOnItemTouchListener extends RecyclerItemClickListener {

    public interface OnMovieTouchListener {
        void onMovieClick(MovieResult movieResult);
    }

    public MoviesOnItemTouchListener(Context context, final RecyclerView recyclerView,
                                     List<MovieResult> movies, OnMovieTouchListener listener) {
        super(context, recyclerView, new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (listener != null && movies != null && movies.get(position) != null) {
                    listener.onMovieClick(movies.get(position));
                }
            }

            @Override
            public void onLongItemClick(View view, int position) {}
        });

    }

}
