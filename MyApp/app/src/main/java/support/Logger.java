package support;

import android.util.Log;

/**
 * Created by filipe.mp on 6/22/17.
 */

public class Logger {

    public static void e(String message) {
        Log.e("ERROR", message);
    }
}
