package support;

import android.app.Application;
import android.arch.persistence.room.Room;

import data.common.local.AppDatabase;

/**
 * Created by filipe.mp on 7/6/17.
 */

public class MyApp extends Application {

    public static AppDatabase database;

    @Override
    public void onCreate() {
        super.onCreate();
        MyApp.database = Room
                .databaseBuilder(this, AppDatabase.class, "seek-n-destroy")
                .build();
    }
}
