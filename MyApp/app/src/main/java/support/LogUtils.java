package support;

import android.util.Log;

/**
 * Created by filipe.mp on 11/2/17.
 */

public class LogUtils {
    private static final String THREAD_LOG = "Thread Logging";

    public static void printCurrentThread() {
        Log.d(THREAD_LOG, "Current thread: " + Thread.currentThread().getName());
    }
}
