package support;

/**
 * Created by filipe.mp on 4/17/17.
 */

public class Constants {
    private final static String baseURL = "https://api.themoviedb.org/3/";

    public static String getBaseURL() {
        return baseURL;
    }
}
