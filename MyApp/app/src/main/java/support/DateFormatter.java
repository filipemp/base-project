package support;

import android.content.Context;
import android.text.format.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by filipe.mp on 6/22/17.
 */

public class DateFormatter {
    private Context context;

    private static final String apiDatePattern = "yyyy-MM-dd";

    public DateFormatter(Context context) {
        this.context = context;
    }

    public String format(String date) {
        return DateUtils.formatDateTime(context, getApiDateMilliseconds(date),
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NUMERIC_DATE
                        | DateUtils.FORMAT_SHOW_YEAR);
    }

    private long getApiDateMilliseconds(String apiDate) {
        Date date = new Date();
        long retVal;

        try {
            date = new SimpleDateFormat(apiDatePattern, Locale.getDefault()).parse(apiDate);
        } catch (ParseException e) {
            Logger.e(e.getMessage());
        } finally {
            retVal = date.getTime();
        }

        return retVal;
    }
}
