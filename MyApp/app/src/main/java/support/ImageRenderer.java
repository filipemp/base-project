package support;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by filipe.mp on 6/21/17.
 */

public class ImageRenderer {

    public static void render(Context context, ImageView targetImageView, String imagePath,
                              int placeholderDrawable, int errorDrawable) {
        Picasso.with(context)
                .load(imagePath)
                .placeholder(placeholderDrawable)
                .error(errorDrawable)
                .into(targetImageView);
    }

    public static void renderWithFit(Context context, ImageView targetImageView, String imagePath,
                                     int placeholderDrawable, int errorDrawable) {

        Picasso.with(context)
                .load(imagePath)
                .placeholder(placeholderDrawable)
                .error(errorDrawable)
                .fit()
                .into(targetImageView);
    }
}
