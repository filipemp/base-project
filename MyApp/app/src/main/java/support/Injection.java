package support;

import data.common.local.AppDatabase;
import data.common.remote.ServiceFactory;
import data.configuration.remote.ConfigurationRemoteDataSource;
import data.configuration.remote.ConfigurationService;
import data.movies.MovieRepository;
import data.movies.MoviesDataContract;
import data.movies.local.MovieLocalDataSource;
import data.movies.remote.MoviesRemoteDataSource;
import data.movies.remote.MoviesService;
import io.reactivex.Maybe;
import presentation.moviedetail.MovieDetailContract;
import presentation.moviedetail.MovieDetailViewModel;
import presentation.movies.MoviesContract;
import presentation.movies.MoviesViewModel;

/**
 * Created by filipe.mp on 6/6/17.
 */

public class Injection {

    public static MovieDetailContract.ViewModel provideMovieDetailViewModel() {
        return new MovieDetailViewModel(provideMoviesRepository());
    }

    public static MoviesContract.ViewModel provideMoviesViewModel() {
        return new MoviesViewModel(provideMoviesRepository());
    }

    private static MoviesDataContract.Repository provideMoviesRepository () {
        return new MovieRepository(provideMoviesLocalDataSource(),
                provideMoviesRemoteDataSource(), provideConfigurationRemoteDataSource());
    }

    private static MoviesDataContract.LocalDataSource provideMoviesLocalDataSource() {
        return new MovieLocalDataSource(provideDatabase());
    }

    private static MoviesDataContract.RemoteDataSource provideMoviesRemoteDataSource() {
        return new MoviesRemoteDataSource(provideMoviesService());
    }

    private static ConfigurationRemoteDataSource provideConfigurationRemoteDataSource() {
        return new ConfigurationRemoteDataSource(provideConfigurationService());
    }

    private static Maybe<ConfigurationService> provideConfigurationService() {
        return ServiceFactory.getServiceMaybe(ConfigurationService.class);
    }

    private static Maybe<MoviesService> provideMoviesService() {
        return ServiceFactory.getServiceMaybe(MoviesService.class);
    }

    private static AppDatabase provideDatabase() {
        return MyApp.database;
    }
}
