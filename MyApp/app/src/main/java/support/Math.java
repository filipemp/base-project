package support;

/**
 * Created by filipe.mp on 6/12/17.
 */

public class Math {

    /***
     * This function puts a given number from the scale 1..10
     * in the scale 1..5
     * */
    public static Double ratingScale(Double value) {
        return (value/10) * 5;
    }
}
