package data.common.local;

/**
 * Created by filipe.mp on 11/2/17.
 */

public class LocalErrors {

    public static Throwable getGenericDatabaseError() {
        return new Throwable("No database available");
    }
}
