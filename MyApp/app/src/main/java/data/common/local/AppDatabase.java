package data.common.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import data.movies.local.models.Genre;
import data.movies.local.models.GenreDao;
import data.movies.local.models.Movie;
import data.movies.local.models.MovieDao;
import data.movies.local.models.MovieGenreJoin;
import data.movies.local.models.MovieGenreJoinDao;

/**
 * Created by filipe.mp on 7/6/17.
 */

@Database(
        entities = { Movie.class, Genre.class, MovieGenreJoin.class },
        version = 1)

public abstract class AppDatabase extends RoomDatabase {

    public abstract MovieDao getMovieDao();
    public abstract GenreDao getGenreDao();
    public abstract MovieGenreJoinDao getMovieGenreJoinDao();

}

