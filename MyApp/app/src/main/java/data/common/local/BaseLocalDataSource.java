package data.common.local;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by filipe.mp on 11/2/17.
 */

public class BaseLocalDataSource {

    protected AppDatabase database;

    public BaseLocalDataSource(AppDatabase database) {
        this.database = database;
    }

    private boolean isDatabaseAvailable() {
        return database != null;
    }

    protected <T> Single<T> getLocalDataSourceSingle(Single<T> single) {
        if (isDatabaseAvailable()) {
            return single.subscribeOn(Schedulers.io());
        }

        return Single.error(LocalErrors.getGenericDatabaseError());
    }

    protected Completable getLocalDataSourceCompletable(Action action) {
        return Completable.create(emitter -> {
            if (isDatabaseAvailable()) {
                action.run();
                emitter.onComplete();
            } else {
                emitter.onError(LocalErrors.getGenericDatabaseError());
            }
        }).subscribeOn(Schedulers.io());
    }
}
