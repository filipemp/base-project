package data.common.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import data.common.remote.models.RemoteError;
import data.common.remote.models.ResponseError;
import retrofit2.Response;
import retrofit2.adapter.rxjava2.Result;

/**
 * Created by filipe.mp on 4/18/17.
 */

public class RemoteErrorParser {

    private static final String NOT_FOUND_MESSAGE = "Your path is wrong, yo!";
    private static final String DEFAULT_MESSAGE = "API generic error";

    private Result result;
    private RemoteError remoteError;

    public RemoteErrorParser(Result result) {
        this.result = result;
    }

    public void decide() {
        setMessages();
    }

    public RemoteError getRemoteError() {
        return this.remoteError;
    }

    private void setMessages() {
        if (result.isError()) {
            setMessageFromResultError();
        } else if (hasErrorBodyOnResponse()) {
            remoteError = new RemoteError(getRemoteErrorMessage());
        } else {
            remoteError = new RemoteError(DEFAULT_MESSAGE);
        }
    }

    private String getRemoteErrorMessage() {
        Gson gson = new GsonBuilder().create();
        ResponseError responseError = new ResponseError();
        try {
            responseError = gson.fromJson(result.response().errorBody().string(), ResponseError.class);
        } catch (IOException exception) {
            responseError.setErrorMessage(NOT_FOUND_MESSAGE);
        }
        return responseError.getErrorMessage();
    }

    private void setMessageFromResultError() {
        remoteError = new RemoteError(result.error().getMessage());
    }

    private boolean hasErrorBodyOnResponse() {
        Response response = result.response();

        return response != null && response.errorBody() != null;
    }
}
