package data.common.remote;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import support.APIConstants;

/**
 * Created by filipe.mp on 4/18/17.
 */

public class RemoteInterceptor implements Interceptor {
    private static final String API_KEY = "api_key";
    private static final String LANGUAGE_KEY = "language";

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request original = chain.request();
        HttpUrl originalHttpUrl = original.url();

        HttpUrl url = originalHttpUrl.newBuilder()
                .addQueryParameter(API_KEY, APIConstants.API_KEY_VALUE)
                .addQueryParameter(LANGUAGE_KEY, APIConstants.LANGUAGE_VALUE)
                .build();

        // Request customization: add request headers
        Request.Builder requestBuilder = original.newBuilder()
                .url(url);

        Request request = requestBuilder.build();
        return chain.proceed(request);
    }

}
