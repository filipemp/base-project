package data.common.remote;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by filipe.mp on 4/17/17.
 */

public class ServiceFactory {

    public static <T> Single<T> getServiceSingle(final Class<T> classService) {
        return new RetrofitBuilder<T>()
                .withClass(classService)
                .buildSingle();
    }

    public static <T> Observable<T> getServiceObservable(final Class<T> classService) {
        return new RetrofitBuilder<T>()
                .withClass(classService)
                .buildObservable();
    }

    public static <T> Maybe<T> getServiceMaybe(final Class<T> classService) {
        return new RetrofitBuilder<T>()
                .withClass(classService)
                .buildMaybe();
    }
}
