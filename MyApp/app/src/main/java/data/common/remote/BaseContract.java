package data.common.remote;

/**
 * Created by filipe.mp on 11/2/17.
 */

public interface BaseContract {

    interface RemoteDataSource {
        boolean isRequestingData();
    }

}
