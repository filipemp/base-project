package data.common.remote.models;

/**
 * Created by filipe.mp on 4/18/17.
 */

public class RemoteError extends Throwable {

    private String message;

    public RemoteError(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
