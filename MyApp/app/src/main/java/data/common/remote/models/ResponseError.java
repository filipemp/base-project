package data.common.remote.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by filipe.mp on 4/18/17.
 */

public class ResponseError {

    @SerializedName("status_message")
    private String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
