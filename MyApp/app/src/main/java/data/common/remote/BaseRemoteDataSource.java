package data.common.remote;

import io.reactivex.Maybe;
import io.reactivex.schedulers.Schedulers;
import retrofit2.adapter.rxjava2.Result;

/**
 * Created by filipe.mp on 4/18/17.
 */

public class BaseRemoteDataSource implements BaseContract.RemoteDataSource {

    private boolean isRequestingData;

    protected <T> Maybe<T> performRequest(Maybe<Result<T>> maybeResponse) {
        return maybeResponse.flatMap(ResponseValidator::convertResultToMaybeModel)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable -> isRequestingData = true)
                .doOnComplete(() -> isRequestingData = false);
    }

    @Override
    public boolean isRequestingData() {
        return isRequestingData;
    }
}
