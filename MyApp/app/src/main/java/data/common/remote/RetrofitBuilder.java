package data.common.remote;

import data.common.remote.models.RemoteError;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import support.Constants;

/**
 * Created by filipe.mp on 4/17/17.
 */
class RetrofitBuilder<T> {

    private static final String NO_SERVICE_CLASS_MESSAGE = "No service interface was used";

    private Class<T> serviceClass = null;

    RetrofitBuilder<T> withClass(final Class<T> serviceClass) {
        this.serviceClass = serviceClass;
        return this;
    }

    Single<T> buildSingle() {
        if (serviceClass == null) {
            Single.error(new RemoteError(NO_SERVICE_CLASS_MESSAGE));
        }

        return Single.just(service());
    }

    Observable<T> buildObservable() {
        if (serviceClass == null) {
            Observable.error(new RemoteError(NO_SERVICE_CLASS_MESSAGE));
        }

        return Observable.just(service());
    }

    Maybe<T> buildMaybe() {
       if (serviceClass == null) {
           Maybe.error(new RemoteError(NO_SERVICE_CLASS_MESSAGE));
       }

       return Maybe.just(service());
    }

    private T service() {
        OkHttpClient okHttpClient = getOkHttpClient();

        return getRetrofit(okHttpClient)
                .create(serviceClass);
    }

    private static Retrofit getRetrofit(OkHttpClient okHttpClient) {
        return getRetrofitBuilder().client(okHttpClient).build();
    }

    private static Retrofit.Builder getRetrofitBuilder() {
        String url = Constants.getBaseURL();

        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(url);
    }

    private static OkHttpClient getOkHttpClient() {
        RemoteInterceptor remoteInterceptor = new RemoteInterceptor();
        return new OkHttpClient.Builder().addInterceptor(remoteInterceptor).build();
    }
}
