package data.common.remote;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.adapter.rxjava2.Result;

/**
 * Created by filipe.mp on 4/18/17.
 */

class ResponseValidator {

    public static <T> Single<T> convertResultToSingleModel(Result<T> result) {

        if (!result.isError() && result.response().isSuccessful()) {
            return Single.just(result.response().body());
        }

        RemoteErrorParser errorDecisionMaker = new RemoteErrorParser(result);
        errorDecisionMaker.decide();

        return Single.error(errorDecisionMaker.getRemoteError());
    }

    public static <T> Observable<T> convertResultToObservableModel(Result<T> result) {

        if (!result.isError() && result.response().isSuccessful()) {
            return Observable.just(result.response().body());
        }

        RemoteErrorParser errorDecisionMaker = new RemoteErrorParser(result);
        errorDecisionMaker.decide();

        return Observable.error(errorDecisionMaker.getRemoteError());
    }

    public static <T> Maybe<T> convertResultToMaybeModel(Result<T> result) {

        if (!result.isError() && result.response().isSuccessful()) {
            return Maybe.just(result.response().body());
        }

        RemoteErrorParser errorDecisionMaker = new RemoteErrorParser(result);
        errorDecisionMaker.decide();

        return Maybe.error(errorDecisionMaker.getRemoteError());
    }
}
