package data.configuration.remote.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by filipe.mp on 6/8/17.
 */

public class ConfigurationResponse {
    @SerializedName("images")
    private ImagesResponse images;

    @SerializedName("change_keys")
    private List<String> changeKeys = null;

    public ImagesResponse getImages() {
        return images;
    }

    public void setImages(ImagesResponse images) {
        this.images = images;
    }

    public List<String> getChangeKeys() {
        return changeKeys;
    }

    public void setChangeKeys(List<String> changeKeys) {
        this.changeKeys = changeKeys;
    }
}
