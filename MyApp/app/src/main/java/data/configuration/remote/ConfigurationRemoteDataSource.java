package data.configuration.remote;

import data.common.remote.BaseRemoteDataSource;
import data.configuration.ConfigurationContract;
import data.configuration.remote.models.ConfigurationResponse;
import io.reactivex.Maybe;

/**
 * Created by filipe.mp on 6/8/17.
 */

public class ConfigurationRemoteDataSource extends BaseRemoteDataSource implements
        ConfigurationContract.RemoteDataSource {

    private Maybe<ConfigurationService> configurationServiceMaybe;

    public ConfigurationRemoteDataSource(Maybe<ConfigurationService> configurationServiceMaybe) {
        this.configurationServiceMaybe = configurationServiceMaybe;
    }

    @Override
    public Maybe<ConfigurationResponse> getConfiguration() {
        return performRequest(configurationServiceMaybe
                .flatMap(ConfigurationService::getConfiguration));
    }
}
