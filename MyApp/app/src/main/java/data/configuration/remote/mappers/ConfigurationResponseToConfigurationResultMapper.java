package data.configuration.remote.mappers;

import java.util.List;

import data.configuration.remote.models.ConfigurationResponse;
import presentation.movies.models.ConfigurationResult;

/**
 * Created by filipe.mp on 6/8/17.
 */

public class ConfigurationResponseToConfigurationResultMapper {

    private final static int BACKDROP_SIZE_POSITION = 2;

    public static ConfigurationResult map(ConfigurationResponse configurationResponse) {
        String posterSize = getImageSize(configurationResponse.getImages().getPosterSizes(),
                configurationResponse.getImages().getPosterSizes().size() - 1);

        String backdropSize = getImageSize(configurationResponse.getImages().getBackdropSizes(),
                BACKDROP_SIZE_POSITION);

        String baseBackdropUrl = getBaseUrl(configurationResponse, backdropSize);
        String basePosterUrl = getBaseUrl(configurationResponse, posterSize);

        return new ConfigurationResult(basePosterUrl, baseBackdropUrl);
    }

    private static String getImageSize(List<String> sizes, int position) {
        String imageSize = sizes.get(0);

        if (sizes.size() > position) {
            imageSize = sizes.get(position);
        }

        return imageSize;
    }

    private static String getBaseUrl(ConfigurationResponse configurationResponse, String imageSize) {
        return configurationResponse.getImages().getSecureBaseUrl() + imageSize;
    }
}
