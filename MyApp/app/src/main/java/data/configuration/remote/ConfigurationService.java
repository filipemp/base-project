package data.configuration.remote;

import data.configuration.remote.models.ConfigurationResponse;
import io.reactivex.Maybe;
import retrofit2.adapter.rxjava2.Result;
import retrofit2.http.GET;

/**
 * Created by filipe.mp on 6/8/17.
 */

public interface ConfigurationService {

    @GET("configuration?")
    Maybe<Result<ConfigurationResponse>> getConfiguration();
}
