package data.configuration;

import data.common.remote.BaseContract;
import data.configuration.remote.models.ConfigurationResponse;
import io.reactivex.Maybe;

/**
 * Created by filipe.mp on 11/2/17.
 */

public interface ConfigurationContract {

    interface RemoteDataSource extends BaseContract.RemoteDataSource {
        Maybe<ConfigurationResponse> getConfiguration();
    }
}
