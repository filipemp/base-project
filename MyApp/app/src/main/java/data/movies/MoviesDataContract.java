package data.movies;

import java.util.List;

import data.common.remote.BaseContract;
import data.movies.local.models.Genre;
import data.movies.local.models.Movie;
import data.movies.remote.models.MovieDetailResponse;
import data.movies.remote.models.NowPlayingRequest;
import data.movies.remote.models.NowPlayingResponse;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;
import presentation.moviedetail.models.MovieDetailResult;
import presentation.movies.models.MovieResult;

/**
 * Created by filipe.mp on 11/2/17.
 */

public interface MoviesDataContract {

    interface LocalDataSource {
        Completable insertMovie(Movie movie, List<Genre> genres);
        Single<List<Movie>> getMovies();
        Single<List<Genre>> getGenresForMovies(int id);
        Completable deleteMovie(Movie movie);
        Single<Movie> getMovie(Integer id);
        Single<Boolean> isSaved(Integer id);
    }

    interface RemoteDataSource extends BaseContract.RemoteDataSource {
        Maybe<NowPlayingResponse> getMoviesNowPlaying(NowPlayingRequest nowPlayingRequest);
        Maybe<MovieDetailResponse> getMovieDetail(String movieId);
    }

    interface Repository {
        Completable saveMovie(MovieDetailResult movieDetailResult);
        Single<List<MovieResult>> getBookmarks();
        Maybe<MovieDetailResult> getMovieDetail(Integer id);
        Maybe<List<MovieResult>> getNowPlayingMovies(Integer page);
        boolean isRequestingData();
        Completable deleteMovie(MovieDetailResult movieDetailResult);
        Single<Boolean> isBookmarked(Integer id);
    }

}
