package data.movies;

import java.util.List;

import data.configuration.ConfigurationContract;
import data.configuration.remote.mappers.ConfigurationResponseToConfigurationResultMapper;
import data.movies.local.mappers.MovieDetailToMovie;
import data.movies.local.mappers.MovieToMovieResult;
import data.movies.remote.mappers.MovieDetailMapper;
import data.movies.remote.mappers.MovieRemoteToMovieDomainMapper;
import data.movies.remote.models.MovieDetailResponse;
import data.movies.remote.models.NowPlayingRequest;
import data.movies.remote.models.NowPlayingResponse;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;
import presentation.moviedetail.models.MovieDetailResult;
import presentation.movies.models.ConfigurationResult;
import presentation.movies.models.MovieResult;

/**
 * Created by filipe.mp on 11/2/17.
 */

public class MovieRepository implements MoviesDataContract.Repository {

    private MoviesDataContract.LocalDataSource moviesLocalDataSource;
    private MoviesDataContract.RemoteDataSource moviesRemoteDataSource;
    private ConfigurationContract.RemoteDataSource configurationRemoteDataSource;

    private ConfigurationResult configurationResult;

    public MovieRepository(MoviesDataContract.LocalDataSource moviesLocalDataSource,
                           MoviesDataContract.RemoteDataSource moviesRemoteDataSource,
                           ConfigurationContract.RemoteDataSource configurationRemoteDataSource) {
        this.moviesLocalDataSource = moviesLocalDataSource;
        this.moviesRemoteDataSource = moviesRemoteDataSource;
        this.configurationRemoteDataSource = configurationRemoteDataSource;
    }

    @Override
    public Maybe<List<MovieResult>> getNowPlayingMovies(Integer page) {
        return configurationRemoteDataSource.getConfiguration()
                .map(ConfigurationResponseToConfigurationResultMapper::map)
                .flatMap(configurationResult -> getNowPlayingMaybe(configurationResult, page))
                .map(nowPlayingResponse -> MovieRemoteToMovieDomainMapper.map(nowPlayingResponse,
                        configurationResult));
    }

    @Override
    public boolean isRequestingData() {
        return configurationRemoteDataSource.isRequestingData() ||
                moviesRemoteDataSource.isRequestingData();
    }

    @Override
    public Single<List<MovieResult>> getBookmarks() {
        return moviesLocalDataSource.getMovies()
                .map(MovieToMovieResult::map);
    }

    @Override
    public Maybe<MovieDetailResult> getMovieDetail(Integer id) {
        return Single.zip(moviesLocalDataSource.getMovie(id),
                moviesLocalDataSource.getGenresForMovies(id), MovieToMovieResult::map)
                .flatMapMaybe(Maybe::just)
                .onErrorResumeNext(getMovieDetailFromRemote(id));
    }

    @Override
    public Completable saveMovie(MovieDetailResult movieDetailResult) {
        return moviesLocalDataSource.insertMovie(MovieDetailToMovie.map(movieDetailResult),
                MovieDetailToMovie.map(movieDetailResult.getGenres()));
    }

    @Override
    public Completable deleteMovie(MovieDetailResult movieDetailResult) {
        return moviesLocalDataSource.deleteMovie(MovieDetailToMovie.map(movieDetailResult));
    }

    @Override
    public Single<Boolean> isBookmarked(Integer id) {
        return moviesLocalDataSource.isSaved(id);
    }

    private Maybe<NowPlayingResponse> getNowPlayingMaybe(ConfigurationResult configurationResult,
                                                         Integer page) {
        this.configurationResult = configurationResult;

        return moviesRemoteDataSource.getMoviesNowPlaying(getParams(page));
    }

    private Maybe<MovieDetailResult> getMovieDetailFromRemote(Integer id) {
        return configurationRemoteDataSource.getConfiguration()
                .map(ConfigurationResponseToConfigurationResultMapper::map)
                .flatMap(configurationResult1 -> getMovieDetailMaybe(configurationResult1, id))
                .map(movieDetailResponse ->
                        MovieDetailMapper.map(movieDetailResponse, configurationResult));
    }

    private NowPlayingRequest getParams(Integer page) {
        return new NowPlayingRequest(String.valueOf(page));
    }

    private Maybe<MovieDetailResponse> getMovieDetailMaybe(ConfigurationResult configurationResult, Integer id) {
        this.configurationResult = configurationResult;

        return moviesRemoteDataSource.getMovieDetail(String.valueOf(id));
    }
}
