package data.movies.local.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by filipe.mp on 11/2/17.
 */

@Entity
public class Genre {

    @PrimaryKey
    public Integer id;

    public String name;

    public Genre(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
}
