package data.movies.local.mappers;

import java.util.ArrayList;
import java.util.List;

import data.movies.local.models.Genre;
import data.movies.local.models.Movie;
import presentation.moviedetail.models.GenreResult;
import presentation.moviedetail.models.MovieDetailResult;

/**
 * Created by filipe.mp on 11/2/17.
 */

public class MovieDetailToMovie {

    public static Movie map(MovieDetailResult movieDetailResult) {
        return new Movie(movieDetailResult.getId(), movieDetailResult.getTitle(),
                movieDetailResult.getPosterPath(), movieDetailResult.getBackdropPath(),
                movieDetailResult.getRating(), movieDetailResult.getOverview(),
                movieDetailResult.getReleaseDate());
    }

    public static List<Genre> map(List<GenreResult> genres) {
        List<Genre> genreList = new ArrayList<>(genres.size());

        for (GenreResult genreResult : genres) {
            genreList.add(map(genreResult));
        }

        return genreList;
    }

    private static Genre map(GenreResult genreResult) {
        return new Genre(genreResult.getId(), genreResult.getName());
    }
}
