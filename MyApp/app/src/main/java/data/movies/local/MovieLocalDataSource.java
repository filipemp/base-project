package data.movies.local;

import java.util.List;

import data.common.local.AppDatabase;
import data.common.local.BaseLocalDataSource;
import data.movies.MoviesDataContract;
import data.movies.local.models.Genre;
import data.movies.local.models.GenreDao;
import data.movies.local.models.Movie;
import data.movies.local.models.MovieDao;
import data.movies.local.models.MovieGenreJoin;
import data.movies.local.models.MovieGenreJoinDao;
import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Created by filipe.mp on 7/6/17.
 */

public class MovieLocalDataSource extends BaseLocalDataSource implements
        MoviesDataContract.LocalDataSource {

    public MovieLocalDataSource(AppDatabase database) {
        super(database);
    }

    @Override
    public Completable insertMovie(Movie movie, List<Genre> genres) {
        return getLocalDataSourceCompletable(() -> {
            getMovieDao().insert(movie);
            getGenreDao().insert(genres);

            for (Genre genre : genres) {
                getMovieGenreJoinDao().insert(new MovieGenreJoin(movie.id, genre.id));
            }
        });
    }

    @Override
    public Single<List<Movie>> getMovies() {
        return getLocalDataSourceSingle(getMovieDao().getMovies());
    }

    @Override
    public Single<List<Genre>> getGenresForMovies(int id) {
        return getLocalDataSourceSingle(getMovieGenreJoinDao().getGenresForMovie(id));
    }

    @Override
    public Completable deleteMovie(Movie movie) {
        return getLocalDataSourceCompletable(() -> getMovieDao().delete(movie));
    }

    @Override
    public Single<Movie> getMovie(Integer id) {
        return getLocalDataSourceSingle(getMovieDao().getMovie(id));
    }

    @Override
    public Single<Boolean> isSaved(Integer id) {
        return getLocalDataSourceSingle(getMovieDao().getMovie(id))
                .map(movie -> true)
                .onErrorResumeNext(throwable -> Single.just(false));
    }

    private MovieDao getMovieDao() {
        return database.getMovieDao();
    }

    private GenreDao getGenreDao() {
        return database.getGenreDao();
    }

    private MovieGenreJoinDao getMovieGenreJoinDao() {
        return database.getMovieGenreJoinDao();
    }
}
