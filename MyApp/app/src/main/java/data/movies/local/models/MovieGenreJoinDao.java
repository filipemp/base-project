package data.movies.local.models;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.RoomWarnings;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by filipe.mp on 11/2/17.
 */

@Dao
public interface MovieGenreJoinDao {
    @Insert
    void insert(MovieGenreJoin movieGenreJoin);

    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @Query("SELECT * FROM genre INNER JOIN movie_genre_join ON " +
            "genre.id=movie_genre_join.genreId WHERE " +
            "movie_genre_join.movieId=:movieId")
    Single<List<Genre>> getGenresForMovie(final int movieId);
}
