package data.movies.local.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by filipe.mp on 7/6/17.
 */

@Entity
public class Movie {

    @PrimaryKey
    public Integer id;

    public String title;

    @ColumnInfo(name = "poster_path")
    public String posterPath;

    @ColumnInfo(name = "backdrop_path")
    public String backdropPath;

    public Double rating;

    public String overview;

    @ColumnInfo(name = "release_date")
    public String releaseDate;

    public Movie(Integer id, String title, String posterPath, String backdropPath, Double rating,
                 String overview, String releaseDate) {
        this.id = id;
        this.title = title;
        this.posterPath = posterPath;
        this.backdropPath = backdropPath;
        this.rating = rating;
        this.overview = overview;
        this.releaseDate = releaseDate;
    }
}
