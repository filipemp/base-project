package data.movies.local.models;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;

import java.util.List;

/**
 * Created by filipe.mp on 11/2/17.
 */

@Dao
public interface GenreDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(List<Genre> genres);

}
