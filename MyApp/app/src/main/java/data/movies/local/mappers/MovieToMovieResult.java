package data.movies.local.mappers;

import java.util.LinkedList;
import java.util.List;

import data.movies.local.models.Genre;
import data.movies.local.models.Movie;
import presentation.moviedetail.models.GenreResult;
import presentation.moviedetail.models.MovieDetailResult;
import presentation.movies.models.MovieResult;

/**
 * Created by filipe.mp on 11/3/17.
 */

public class MovieToMovieResult {

    public static List<MovieResult> map(List<Movie> movies) {
        List<MovieResult> movieResultList = new LinkedList<>();

        for (Movie movie : movies) {
            movieResultList.add(map(movie));
        }

        return movieResultList;
    }

    private static MovieResult map(Movie movie) {
        return new MovieResult(movie.posterPath, movie.overview, movie.id, movie.title,
                movie.rating, movie.backdropPath);
    }

    public static MovieDetailResult map(Movie movie, List<Genre> genres) {
        List<GenreResult> genreResults = new LinkedList<>();

        for (Genre genre : genres) {
            genreResults.add(map(genre));
        }

        return new MovieDetailResult(movie.id, movie.title, movie.posterPath, movie.backdropPath,
                movie.rating, movie.overview, movie.releaseDate, genreResults);
    }

    private static GenreResult map(Genre genre) {
        return new GenreResult(genre.id, genre.name);
    }
}
