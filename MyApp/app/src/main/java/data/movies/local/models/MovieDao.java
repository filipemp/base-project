package data.movies.local.models;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by filipe.mp on 7/6/17.
 */

@Dao
public interface MovieDao {

    @Query("SELECT * from movie")
    Single<List<Movie>> getMovies();

    @Insert
    void insert(Movie movie);

    @Delete
    void delete(Movie movie);

    @Query("SELECT * FROM movie WHERE id=:id")
    Single<Movie> getMovie(Integer id);
}
