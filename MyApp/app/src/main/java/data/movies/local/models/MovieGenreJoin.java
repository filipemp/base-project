package data.movies.local.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;

import org.jetbrains.annotations.NotNull;

import static android.arch.persistence.room.ForeignKey.CASCADE;

/**
 * Created by filipe.mp on 11/2/17.
 */

@Entity(tableName = "movie_genre_join",
        primaryKeys = { "movieId", "genreId" },
        foreignKeys = {
                @ForeignKey(entity = Movie.class,
                        parentColumns = "id",
                        childColumns = "movieId",
                        onDelete = CASCADE),
                @ForeignKey(entity = Genre.class,
                        parentColumns = "id",
                        childColumns = "genreId")},
        indices = { @Index(value = "movieId")
                        })
public class MovieGenreJoin {

    @NonNull
    public final Integer movieId;

    @NonNull
    public final Integer genreId;

    public MovieGenreJoin(@NotNull Integer movieId, @NotNull Integer genreId) {
        this.movieId = movieId;
        this.genreId = genreId;
    }
}
