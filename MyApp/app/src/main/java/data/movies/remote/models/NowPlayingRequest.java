package data.movies.remote.models;

/**
 * Created by filipe.mp on 4/18/17.
 */

public class NowPlayingRequest {
    private String page;

    public NowPlayingRequest(String page) {
        this.page = page;
    }

    public String getPage() {
        return page;
    }
}
