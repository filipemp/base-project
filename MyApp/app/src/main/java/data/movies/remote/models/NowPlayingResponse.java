package data.movies.remote.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by filipe.mp on 4/18/17.
 */

public class NowPlayingResponse {

    @SerializedName("results")
    private List<MovieResponse> movies;

    @SerializedName("total_pages")
    private Integer totalPages;

    public List<MovieResponse> getMovies() {
        return movies;
    }

    public void setMovies(List<MovieResponse> movies) {
        this.movies = movies;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }
}
