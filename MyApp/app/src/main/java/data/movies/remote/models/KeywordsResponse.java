package data.movies.remote.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by filipe.mp on 4/23/17.
 */

public class KeywordsResponse {

    @SerializedName("keywords")
    private List<KeywordResponse> keywordResponseList;

    public List<KeywordResponse> getKeywordResponseList() {
        return keywordResponseList;
    }
}
