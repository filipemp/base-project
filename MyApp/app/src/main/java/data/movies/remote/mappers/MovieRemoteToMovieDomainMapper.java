package data.movies.remote.mappers;

import java.util.ArrayList;
import java.util.List;

import data.movies.remote.models.MovieResponse;
import data.movies.remote.models.NowPlayingResponse;
import presentation.movies.models.ConfigurationResult;
import presentation.movies.models.MovieResult;
import support.Math;

/**
 * Created by filipe.mp on 4/23/17.
 */

public class MovieRemoteToMovieDomainMapper {

    public static List<MovieResult> map(NowPlayingResponse nowPlayingResponse,
                                        ConfigurationResult configurationResult) {
        if (nowPlayingResponse.getMovies() == null || nowPlayingResponse.getMovies().isEmpty()) {
            // return error
        }

        List<MovieResult> movies = new ArrayList<>();
        List<MovieResponse> moviesResponse = nowPlayingResponse.getMovies();

        for (MovieResponse movieResponse : moviesResponse) {
            if (isMovieValid(movieResponse)) {
                MovieResult movieResult = MovieRemoteToMovieDomainMapper.map(
                        movieResponse,
                        configurationResult.getBasePosterUrl(),
                        configurationResult.getBaseBackdropUrl());
                movies.add(movieResult);
            }
        }

        return movies;
    }

    private static boolean isMovieValid(MovieResponse movieResponse) {
        return movieResponse.getBackdropPath() != null
                && movieResponse.getPosterPath() != null
                && movieResponse.getOverview() != null
                && movieResponse.getTitle() != null;
    }

    private static MovieResult map(MovieResponse movie, String basePosterUrl,
                                   String baseBackdropUrl) {

        return new MovieResult(
                basePosterUrl + movie.getPosterPath(),
                movie.getOverview(),
                movie.getId(),
                movie.getTitle(),
                Math.ratingScale(movie.getVoteAverage()),
                baseBackdropUrl + movie.getBackdropPath()
        );
    }

}
