package data.movies.remote;

import data.movies.remote.models.KeywordsResponse;
import data.movies.remote.models.MovieDetailResponse;
import data.movies.remote.models.NowPlayingResponse;
import io.reactivex.Maybe;
import retrofit2.adapter.rxjava2.Result;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by filipe.mp on 4/18/17.
 */

public interface MoviesService {

    @GET("movie/now_playing?")
    Maybe<Result<NowPlayingResponse>> getMoviesNowPlaying(@Query("page") String page);

    @GET("movie/{movie_id}/keywords")
    Maybe<Result<KeywordsResponse>> getMovieKeywords(@Path("movie_id") String movieId);

    @GET("movie/{movie_id}")
    Maybe<Result<MovieDetailResponse>> getMovieDetails(@Path("movie_id") String movieId);
}
