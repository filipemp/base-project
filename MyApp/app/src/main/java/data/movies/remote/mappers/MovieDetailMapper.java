package data.movies.remote.mappers;

import java.util.ArrayList;
import java.util.List;

import data.movies.remote.models.GenreResponse;
import data.movies.remote.models.MovieDetailResponse;
import presentation.moviedetail.models.GenreResult;
import presentation.moviedetail.models.MovieDetailResult;
import presentation.movies.models.ConfigurationResult;
import support.Math;

/**
 * Created by filipe.mp on 6/12/17.
 */

public class MovieDetailMapper {

    public static MovieDetailResult map(MovieDetailResponse movieDetailResponse, ConfigurationResult configurationResult) {
        String posterFullPath = configurationResult.getBasePosterUrl() + movieDetailResponse.getPosterPath();
        String backdropFullPath = configurationResult.getBaseBackdropUrl() + movieDetailResponse.getBackdropPath();

        return new MovieDetailResult(movieDetailResponse.getId(), movieDetailResponse.getTitle(),
                posterFullPath, backdropFullPath,
                Math.ratingScale(movieDetailResponse.getVoteAverage()),
                movieDetailResponse.getOverview(), movieDetailResponse.getReleaseDate(), 
                map(movieDetailResponse.getGenres()));
    }
    
    private static List<GenreResult> map(List<GenreResponse> genreResponseList) {
        List<GenreResult> retVal = new ArrayList<>();
        
        for (GenreResponse genreResponse : genreResponseList) {
            retVal.add(map(genreResponse));
        }
        
        return retVal;
    }

    private static GenreResult map(GenreResponse genreResponse) {
        return new GenreResult(genreResponse.getId(), genreResponse.getName());
    }
}
