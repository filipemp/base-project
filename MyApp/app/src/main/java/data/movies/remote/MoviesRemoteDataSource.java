package data.movies.remote;

import data.common.remote.BaseRemoteDataSource;
import data.movies.MoviesDataContract;
import data.movies.remote.models.MovieDetailResponse;
import data.movies.remote.models.NowPlayingRequest;
import data.movies.remote.models.NowPlayingResponse;
import io.reactivex.Maybe;

/**
 * Created by filipe.mp on 4/18/17.
 */

public class MoviesRemoteDataSource extends BaseRemoteDataSource implements
        MoviesDataContract.RemoteDataSource {

    private Maybe<MoviesService> moviesServiceMaybe;

    public MoviesRemoteDataSource(Maybe<MoviesService> moviesServiceMaybe) {
        this.moviesServiceMaybe = moviesServiceMaybe;
    }

    @Override
    public Maybe<NowPlayingResponse> getMoviesNowPlaying(NowPlayingRequest nowPlayingRequest) {
        return performRequest(moviesServiceMaybe.flatMap(moviesService ->
                moviesService.getMoviesNowPlaying(nowPlayingRequest.getPage())));
    }

    @Override
    public Maybe<MovieDetailResponse> getMovieDetail(String movieId) {
        return performRequest(moviesServiceMaybe.flatMap(moviesService ->
        moviesService.getMovieDetails(movieId)));
    }

}
