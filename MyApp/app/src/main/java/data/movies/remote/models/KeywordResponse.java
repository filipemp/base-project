package data.movies.remote.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by filipe.mp on 4/23/17.
 */

public class KeywordResponse {

    @SerializedName("id")
    private Integer id;

    @SerializedName("name")
    private String keyword;

    public KeywordResponse(Integer id, String keyword) {
        this.id = id;
        this.keyword = keyword;
    }

    public Integer getId() {
        return id;
    }

    public String getKeyword() {
        return keyword;
    }
}
