package data.movies.remote.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by filipe.mp on 6/9/17.
 */

public class SpokenLanguageResponse {

    @SerializedName("iso_639_1")
    public String iso6391;
    @SerializedName("name")
    public String name;

    public String getIso6391() {
        return iso6391;
    }

    public void setIso6391(String iso6391) {
        this.iso6391 = iso6391;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
