package data.movies.remote.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by filipe.mp on 6/9/17.
 */

public class ProductionCompanyResponse {

    @SerializedName("name")
    public String name;
    @SerializedName("id")
    public Integer id;
}
