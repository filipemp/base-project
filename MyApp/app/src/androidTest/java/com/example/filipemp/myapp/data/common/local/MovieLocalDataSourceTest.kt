package com.example.filipemp.myapp.data.common.local

import android.support.test.runner.AndroidJUnit4
import data.common.local.LocalErrors
import data.movies.local.MovieLocalDataSource
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.any
import support.MyApp

@RunWith(AndroidJUnit4::class)
class MovieLocalDataSourceTest {

    private val movieLocalDataSource = MovieLocalDataSource(MyApp.database)
    private val movieLocalDataSourceWithoutDabase = MovieLocalDataSource(null)

    @Test
    fun testInsertMovie_Success() {
        val testObserver = movieLocalDataSource.insertMovie(any(), any()).test()

        testObserver.assertNoErrors()
    }

    @Test
    fun testInsertMovie_Failure() {
        val testObserver = movieLocalDataSourceWithoutDabase.insertMovie(any(), any()).test()

        testObserver.assertError(LocalErrors.getGenericDatabaseError()::class.java)
    }
}